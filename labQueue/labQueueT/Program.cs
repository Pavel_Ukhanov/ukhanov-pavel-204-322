﻿using System;
using System.Collections.Generic;

namespace labQueueT
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<int> x = new Queue<int>();
            x.Enqueue(7);
            x.Enqueue(4);
            x.Enqueue(2);

            Console.WriteLine(x.Peek());
            Console.WriteLine("---");

            int sum = 0;
            while (x.Count > 0)
            {
                int v = x.Dequeue();
                sum += v;
                Console.WriteLine(v);
            }

            Console.WriteLine("---");
            Console.WriteLine($"sum = {sum}");
        }
    }
}
