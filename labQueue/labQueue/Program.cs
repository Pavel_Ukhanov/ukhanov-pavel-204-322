﻿using System;
using System.Collections;

namespace labQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue x = new Queue();
            x.Enqueue(12345);
            x.Enqueue("hello");
            x.Enqueue('y');
            x.Enqueue(new Queue());

            Console.WriteLine(x.Peek()); 
            Console.WriteLine("---");
            while (x.Count > 0)
            {
                Console.WriteLine(x.Dequeue()); 
            }
        }
    }
}
