﻿using System.Drawing;
using System.Windows.Forms;
using System.Threading;

namespace labImageScrollHorz
{
    public partial class Fm : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        private Bitmap imgBG;
        private Bitmap imgEarth;
        private Point startPoint;
        private int deltaX = 0;
        private int deltaXs = 0;
        private int stackImgs = 0;
        private int secondStackImgs = 0;

        private int speed;
        private readonly int defaultSpeed;

        private System.Threading.Timer timer;

        public bool IsDown { get; private set; }

        public Fm()
        {
            InitializeComponent();

            defaultSpeed = 2;
            speed = defaultSpeed;

            imgBG = Properties.Resources.fon;
            imgEarth = Properties.Resources.earth;
            Height = imgBG.Height;
            IsDown = false;

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);

            DoubleBuffered = true;
            Paint += (s, e) => { UpdateBG();  e.Graphics.DrawImage(b, 0, 0); };
            MouseDown += (s, e) => startPoint = e.Location;
            MouseMove += Fm_MouseMove;
            KeyDown += Fm_KeyDown;
            KeyUp += Fm_KeyUp;
        }

        private void Fm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left || e.KeyCode == Keys.Right)
            {
                timer?.Dispose();
                speed = defaultSpeed;
                timer = null;
            }
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    IsDown = true;
                    UpdateDeltaX(speed);
                    UpdateDeltaXs(speed + 20);

                    if (timer == null)
                    {
                        timer = new System.Threading.Timer(OnTimeOut, null, 2000, 0);
                    }
                    
                    break;
                case Keys.Right:
                    IsDown = true;
                    UpdateDeltaX(-speed);
                    UpdateDeltaXs(-speed - 20);

                    if (timer == null)
                    {
                        timer = new System.Threading.Timer(OnTimeOut, null, 2000, 0);
                    }

                    break;
            }

            Invalidate();
        }

        private void Fm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                UpdateDeltaX(e.X - startPoint.X);
                UpdateDeltaXs(e.X - startPoint.X);
                startPoint = e.Location;
                Invalidate();
            }
        }

        private void UpdateDeltaX(int v)
        {
            Text = $"{Application.ProductName} : deltaX={deltaX}, v={v}";
            deltaX += v;

            if (deltaX > 0)
                deltaX -= imgBG.Width;
            else
                if (deltaX < -imgBG.Width)
                deltaX += imgBG.Width;
        }

        private void UpdateDeltaXs(int v)
        {
            deltaXs += v;

            if (deltaXs > 0)
                deltaXs -= imgEarth.Width;
            else
                if (deltaXs < -imgEarth.Width)
                deltaXs += imgBG.Width;
        }

        private void UpdateBG()
        {
            stackImgs = (ActiveForm.Width / imgBG.Width) + 1;

            if (stackImgs == 1)
            {
                stackImgs++;
            }
            
            for (int i = 0; i < stackImgs; i++)
            {
                g.DrawImage(imgBG, deltaX + i * imgBG.Width, 0);
            }

            secondStackImgs = (ActiveForm.Width / imgEarth.Width) + 1;

            if (secondStackImgs == 1)
            {
                secondStackImgs++;
            }

            for (int i = 0; i < secondStackImgs; i++)
            {
                g.DrawImage(imgEarth, deltaXs + i * imgEarth.Width, imgBG.Height - imgEarth.Height / 2);
            }
        }

        private void OnTimeOut(object param)
        {
            timer?.Dispose();
            speed += 8;
        }
    }
}
