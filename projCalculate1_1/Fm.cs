﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Fm : Form
    {
        double FirstValue = 0, SecondValue = 0, BufSecondValue = 0;
        bool SInput = false;
        string Sign = null;

        public Fm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)//9
        {
            if (SInput == true || textbox.Text == "0" || textbox.Text == "На 0 делить нельзя" || button17.Enabled == false)
            {
                textbox.Clear();
                SInput = false;

                if (button17.Enabled == false)
                {
                    button17.Enabled = true;
                }
            }

            if (textbox.TextLength < 16)
            {
                textbox.Text = textbox.Text + 9;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)//8
        {
            if (SInput == true || textbox.Text == "0" || textbox.Text == "На 0 делить нельзя" || button17.Enabled == false)
            {
                textbox.Clear();
                SInput = false;

                if (button17.Enabled == false)
                {
                    button17.Enabled = true;
                }
            }

            if (textbox.TextLength < 16)
            {
                textbox.Text = textbox.Text + 8;
            }
        }

        private void button2_Click(object sender, EventArgs e)//7
        {
            if (SInput == true || textbox.Text == "0" || textbox.Text == "На 0 делить нельзя" || button17.Enabled == false)
            {
                textbox.Clear();
                SInput = false;

                if (button17.Enabled == false)
                {
                    button17.Enabled = true;
                }
            }


            if (textbox.TextLength < 16)
            {
                textbox.Text = textbox.Text + 7;
            }
        }

        private void button3_Click(object sender, EventArgs e)//4
        {
            if (SInput == true || textbox.Text == "0" || textbox.Text == "На 0 делить нельзя" || button17.Enabled == false)
            {
                textbox.Clear();
                SInput = false;

                if (button17.Enabled == false)
                {
                    button17.Enabled = true;
                }
            }

            if (textbox.TextLength < 16)
            {
                textbox.Text = textbox.Text + 4;
            }
        }

        private void button4_Click(object sender, EventArgs e)//5
        {
            if (SInput == true || textbox.Text == "0" || textbox.Text == "На 0 делить нельзя" || button17.Enabled == false)
            {
                textbox.Clear();
                SInput = false;

                if (button17.Enabled == false)
                {
                    button17.Enabled = true;
                }
            }

            if (textbox.TextLength < 16)
            {
                textbox.Text = textbox.Text + 5;
            }

        }

        private void button5_Click(object sender, EventArgs e)//6
        {

            if (SInput == true || textbox.Text == "0" || textbox.Text == "На 0 делить нельзя" || button17.Enabled == false)
            {
                textbox.Clear();
                SInput = false;

                if (button17.Enabled == false)
                {
                    button17.Enabled = true;
                }
            }

            if (textbox.TextLength < 16)
            {
                textbox.Text = textbox.Text + 6;
            }
        }

        private void button6_Click(object sender, EventArgs e)//1
        {
            if (SInput == true || textbox.Text == "0" || textbox.Text == "На 0 делить нельзя" || button17.Enabled == false)
            {
                textbox.Clear();
                SInput = false;

                if (button17.Enabled == false)
                {
                    button17.Enabled = true;
                }
            }

            if (textbox.TextLength < 16)
            {
                textbox.Text = textbox.Text + 1;
            }

        }

        private void button7_Click(object sender, EventArgs e)//2
        {
            if (SInput == true || textbox.Text == "0" || textbox.Text == "На 0 делить нельзя" || button17.Enabled == false)
            {
                textbox.Clear();
                SInput = false;

                if (button17.Enabled == false)
                {
                    button17.Enabled = true;
                }
            }

            if (textbox.TextLength < 16)
            {
                textbox.Text = textbox.Text + 2;
            }
        }

        private void button8_Click(object sender, EventArgs e)//3
        {
            if (SInput == true || textbox.Text == "0" || textbox.Text == "На 0 делить нельзя" || button17.Enabled == false)
            {
                textbox.Clear();
                SInput = false;

                if (button17.Enabled == false)
                {
                    button17.Enabled = true;
                }
            }

            if (textbox.TextLength < 16)
            {
                textbox.Text = textbox.Text + 3;
            }

        }

        private void button14_Click(object sender, EventArgs e)//C
        {
            FirstValue = 0;
            SecondValue = 0;
            textbox.Text = "0";
            SInput = false;
            Sign = null;

            if (button17.Enabled == false)
            {
                button17.Enabled = true;
            }
        }

        private void button13_Click(object sender, EventArgs e)//0
        {
            if (SInput == true || textbox.Text == "0" || textbox.Text == "На 0 делить нельзя" || button17.Enabled == false)
            {
                textbox.Clear();
                SInput = false;

                if (button17.Enabled == false)
                {
                    button17.Enabled = true;
                }
            }

            if (textbox.TextLength < 16)
            {
                textbox.Text = textbox.Text + 0;
            }
        }

        private void button12_Click(object sender, EventArgs e)//.
        {
            if (SInput == true)
            {
                textbox.Clear();
                textbox.Text = textbox.Text + 0;
                SInput = false;
            }

            if (textbox.Text == "На 0 делить нельзя")
            {
                textbox.Clear();
                textbox.Text = textbox.Text + 0;
            }

            if (CheckDot() == false && textbox.TextLength < 16)
            {
                textbox.Text = textbox.Text + ",";
            }
        }

        private void button15_Click(object sender, EventArgs e)//=
        {
            if (textbox.Text != "На 0 делить нельзя")
            {
                Calculate();
                SInput = false;
            }
        }

        private void button16_Click(object sender, EventArgs e)// /
        {
            if (textbox.Text != "На 0 делить нельзя")
            {
                FirstValue = Convert.ToDouble(textbox.Text);
                Sign = "/";
                SInput = true;

                if (button17.Enabled == false)
                {
                    button17.Enabled = true;
                }
            }
        }

        private void button17_Click(object sender, EventArgs e)// <-
        {
            if (textbox.TextLength > 1 && SInput == false)
            {
                int Length;
                string Text;

                Length = textbox.TextLength - 1;
                Text = textbox.Text;
                textbox.Clear();

                for (int i = 0; i < Length; i++)
                {
                    textbox.Text = textbox.Text + Text[i];
                }
            }
        }

        private void button9_Click(object sender, EventArgs e)//+
        {
            if (textbox.Text != "На 0 делить нельзя")
            {
                FirstValue = Convert.ToDouble(textbox.Text);
                Sign = "+";
                SInput = true;

                if (button17.Enabled == false)
                {
                    button17.Enabled = true;
                }
            }

        }

        private void button10_Click(object sender, EventArgs e)//-
        {
            if (textbox.Text != "На 0 делить нельзя")
            {
                FirstValue = Convert.ToDouble(textbox.Text);
                Sign = "-";
                SInput = true;

                if (button17.Enabled == false)
                {
                    button17.Enabled = true;
                }
            }

        }

        private void button11_Click(object sender, EventArgs e) // *
        {
            if (textbox.Text != "На 0 делить нельзя")
            {
                FirstValue = Convert.ToDouble(textbox.Text);
                Sign = "*";
                SInput = true;

                if (button17.Enabled == false)
                {
                    button17.Enabled = true;
                }
            }
        }

        protected override void OnKeyDown(KeyEventArgs kea)
        {

            switch (kea.KeyCode)
            {
                case Keys.NumPad0:
                case Keys.D0:
                    {
                        button13.PerformClick();
                        break;
                    }

                case Keys.NumPad1:
                case Keys.D1:
                    {
                        button6.PerformClick();
                        break;
                    }
                case Keys.NumPad2:
                case Keys.D2:
                    {
                        button7.PerformClick();
                        break;
                    }

                case Keys.NumPad3:
                case Keys.D3:
                    {
                        button8.PerformClick();
                        break;
                    }

                case Keys.NumPad4:
                case Keys.D4:
                    {
                        button3.PerformClick();
                        break;
                    }

                case Keys.NumPad5:
                case Keys.D5:
                    {
                        button4.PerformClick();
                        break;
                    }

                case Keys.NumPad6:
                case Keys.D6:
                    {
                        button5.PerformClick();
                        break;
                    }

                case Keys.NumPad7:
                case Keys.D7:
                    {
                        button6.PerformClick();
                        break;
                    }

                case Keys.NumPad8:
                case Keys.D8:
                    {
                        button1.PerformClick();
                        break;
                    }

                case Keys.NumPad9:
                case Keys.D9:
                    {
                        button2.PerformClick();
                        break;
                    }

                case Keys.Decimal:
                    {
                        button12.PerformClick();
                        break;
                    }

                case Keys.Add:
                    {
                        button9.PerformClick();
                        break;
                    }


                case Keys.Multiply:
                    {
                        button11.PerformClick();
                        break;
                    }

                case Keys.Divide:
                    {
                        button16.PerformClick();
                        break;
                    }

                case Keys.Subtract:
                    {
                        button10.PerformClick();
                        break;
                    }

                case Keys.Insert:
                    {
                        button15.PerformClick();
                        break;
                    }

                case Keys.Delete:
                    {
                        button14.PerformClick();
                        break;
                    }

                case Keys.Back:
                    {
                        button17.PerformClick();
                        break;
                    }
            }
        }

        protected override void OnShown(EventArgs ea)
        {
            button15.Focus();
        }

        void Lostfocus(object obj, EventArgs ea)
        {
            Button button = (Button)obj;
            button.Focus();
        }

        private void Calculate()
        {
            if (Sign == "error")
            {
                return;
            }

            if (button17.Enabled != false)
            {
                SecondValue = Convert.ToDouble(textbox.Text);
                BufSecondValue = SecondValue;
            }
            else
            {
                if (Sign != null)
                {
                    SecondValue += BufSecondValue;
                }
            }


            switch (Sign)
            {

                case "+":
                    {
                        textbox.Text = (FirstValue + SecondValue).ToString("g");
                        break;
                    }

                case "*":
                    {
                        textbox.Text = (FirstValue * SecondValue).ToString("g");
                        break;
                    }

                case "/":
                    {
                        if (SecondValue == 0)
                        {
                            textbox.Text = "На 0 делить нельзя";
                            button9.Enabled = false;
                            button10.Enabled = false;
                            button11.Enabled = false;
                            button15.Enabled = false;
                            button16.Enabled = false;

                        }
                        else
                        {
                            textbox.Text = (FirstValue / SecondValue).ToString("g");
                        }

                        break;
                    }

                case "-":
                    {
                        textbox.Text = (FirstValue - SecondValue).ToString("g");
                        break;
                    }
            }

            button17.Enabled = false;
        }

        private bool CheckDot()
        {
            for (int i = 0; i < textbox.TextLength; i++)
            {
                if (textbox.Text[i] == ',')
                {
                    return true;
                }
            }

            return false;
        }
    }
}
