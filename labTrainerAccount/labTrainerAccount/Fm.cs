﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labTrainerAccount
{
    public partial class Fm : Form
    {
        private readonly Game g;

        public Fm()
        {
            InitializeComponent();

            g = new Game();
            g.Change += G_Change;
            g.DoReset();

            buYes.Click += (s, e) => g.DoAnswer(true);
            buNo.Click += (s, e) => g.DoAnswer(false);
            btnReset.Click += (s, e) => g.DoReset();
            btnMiss.Click += (s, e) => g.DoContinue(g.CountCorrect);
        }

        private void G_Change(object sender, EventArgs e)
        {
            laCorrect.Text = $"Верно = {g.CountCorrect}";
            laWrong.Text = $"Неверно = {g.CountWrong}";
            laCode.Text = g.CodeText;
            lvlHard.Text = g.LevelHard;
        }

        private void Fm_Load(object sender, EventArgs e)
        {

        }

    }
}
