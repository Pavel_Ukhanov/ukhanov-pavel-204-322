﻿using System;

namespace labTrainerAccount
{
    internal class Game
    {
        public int CountCorrect { get; private set; }
        public int CountWrong { get; private set; }
        public string LevelHard { get; private set; }
        public string CodeText { get; private set; }
        public event EventHandler Change;
        private bool answerCorrect;

        internal void DoReset()
        {
            CountCorrect = 0;
            CountWrong = 0;

            DoContinue(0);
        }

        public void DoContinue(int countAnswer)
        {
            Random rnd = new Random();
            int xValue1 = 0;
            int xValue2 = 0;
            if (countAnswer < 10)
            {
                LevelHard = "Уровень сложности 1";
                xValue1 = rnd.Next(20);
                xValue2 = rnd.Next(20);
            } else if (countAnswer > 10 && countAnswer < 20)
            {
                LevelHard = "Уровень сложности 2";
                xValue1 = rnd.Next(40);
                xValue2 = rnd.Next(40);
            } else
            {
                LevelHard = "Уровень сложности 3";
                xValue1 = rnd.Next(60);
                xValue2 = rnd.Next(60);
            }
           
            int sign = rnd.Next(1,4);
            string signString = "";
            float xResult = 0;

            switch (sign)
            {
                case 1:
                    signString = "+";
                    xResult = xValue1 + xValue2;
                    break;
                case 2:
                    signString = "-";
                    xResult = xValue1 - xValue2;
                    break;
                case 3:
                    signString = "*";
                    xResult = xValue1 * xValue2;
                    break;
                case 4:
                    signString = "/";
                    xResult = xValue1 / xValue2;
                    break;
            }

            float xResultFake = xResult;

            if (rnd.Next(2) == 1)
                xResultFake += rnd.Next(1, 7) * (rnd.Next(2) == 1 ? 1 : -1);
            answerCorrect = xResult == xResultFake;
            CodeText = $"{xValue1} {signString} {xValue2} = {xResultFake}";
            Change?.Invoke(this, EventArgs.Empty);
        }

        public void DoAnswer(bool v)
        {
            if (v == answerCorrect)
                CountCorrect++;
            else
                CountWrong++;
            DoContinue(CountCorrect);
        }
    }
}