﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms;

namespace averbrightness
{
    public partial class Fm : Form
    {
        OpenFileDialog opendlg = new OpenFileDialog();
        SaveFileDialog savedlg = new SaveFileDialog();
        bool notsaved = false;
        Bitmap pict;
        Bitmap oldpict;
        List<double> oldbrtlist = new List<double>();
        string lastfile;
        PrintDocument Document;
        public Fm()
        {
            InitializeComponent();
            opendlg.InitialDirectory = Application.StartupPath;
            opendlg.CheckFileExists = true;
            opendlg.CheckPathExists = true;
            opendlg.Title = "Открытие файла";
            opendlg.Multiselect = false;
            opendlg.DefaultExt = ".jpg";
            opendlg.AddExtension = true;
            opendlg.Filter = "Графические файлы | *.jpg; *.jpeg; *.png; *.bmp; *.tiff";
            savedlg.AddExtension = true;
            savedlg.CheckPathExists = true;
            savedlg.DefaultExt = ".jpg";
            savedlg.Filter = "Графические файлы | *.jpg; *.jpeg; *.png; *.bmp; *.tiff";
            savedlg.Title = "Сохранение файла";
        }

        public void OpenFileMenu_Click(object sender, EventArgs e)
        {
            opendlg.FileName = "";
            if (notsaved)
            {
                DialogResult choice = MessageBox.Show(this, "Вы хотите сохранить файл?", "Сохранение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                switch (choice)
                {
                    case DialogResult.Yes:
                        SaveFileMenuClick(sender, e);
                        break;
                    case DialogResult.No:
                        break;
                }
            }
            if (opendlg.ShowDialog() == DialogResult.OK)
            {
                opendlg.InitialDirectory = opendlg.FileName;
                try
                {
                    Image tmpimg;
                    pict = new Bitmap(tmpimg=Image.FromFile(opendlg.FileName));
                    oldpict = new Bitmap(pict);
                    tmpimg.Dispose();
                    pictureBox1.Image = pict;
                }
                catch (OutOfMemoryException)
                {
                    MessageBox.Show(this, "Неправильный формат файла!", "Ошибка");
                    return;
                }
                catch(FileNotFoundException)
                {
                    MessageBox.Show(this, "Файл не существует!", "Ошибка");
                    return;
                }
                lastfile = opendlg.FileName;
                trackBar1.Enabled = true;
                button1.Enabled = true;
                notsaved = false;
                oldbrtlist.Clear();
                button2.Enabled = false;
                trackBar1.Value = 0;
            }         
        }

        private void SaveAsFileMenuClick(object sender, EventArgs e)
        {
            if (pict == null)
                return;
            savedlg.FileName = "";
            if (savedlg.ShowDialog() == DialogResult.OK)
            {
                bool checkext = savedlg.FileName.EndsWith(".jpg") 
                    || savedlg.FileName.ToLower().EndsWith(".jpeg") 
                    || savedlg.FileName.ToLower().EndsWith(".png") 
                    || savedlg.FileName.ToLower().EndsWith(".bmp") 
                    || savedlg.FileName.ToLower().EndsWith(".tiff");
                if(!checkext)
                {
                    MessageBox.Show(this, "Указано неверное расширение файла", "Ошибка");
                    return;
                }
                lastfile = savedlg.FileName;
                savedlg.InitialDirectory = savedlg.FileName;
                pict.Save(savedlg.FileName);
              
                trackBar1.Value = 0;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (notsaved)
            {
                DialogResult choice = MessageBox.Show(this, "Вы хотите сохранить файл?", "Сохранение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                switch (choice)
                {
                    case DialogResult.Yes:
                        SaveFileMenuClick(sender, e);
                        break;
                    case DialogResult.No:
                        return;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            if (oldpict == null || pict == null)
                    return;
                if (trackBar1.Value == 0)
                    return;
                double k = 1 + Math.Abs(trackBar1.Value) / 100d;
                k = (trackBar1.Value < 0) ? 1 / k : k;
                oldbrtlist.Add(k);
                double resultk = 1;
                foreach (var s in oldbrtlist)
                    resultk *= s;
                changebrt(resultk);
                pictureBox1.Image = pict;
                button2.Enabled = true;
                trackBar1.Value = 0;
                notsaved = true;
                panel1.Visible = false;
                panel2.Visible = false;
                    
        }

        private void changebrt(double k)
        {
            if (oldpict == null || pict==null)
                return;
            if (k == 1)
            {
                pict = new Bitmap(oldpict);
                return;
            }
            for (int i = 0; i < pict.Width; ++i)
                for (int j = 0; j < pict.Height; ++j)
                {
                    double tmpk = k;
                    Color tmppixel = oldpict.GetPixel(i, j);
                    if (tmpk > 1 && tmpk * Math.Max(Math.Max(tmppixel.R, tmppixel.G), tmppixel.B) > 255)
                        tmpk = 255d / Math.Max(Math.Max(tmppixel.R, tmppixel.G), tmppixel.B);
                    pict.SetPixel(i, j, Color.FromArgb((int)Math.Round(tmppixel.R * tmpk), (int)Math.Round(tmppixel.G * tmpk), (int)Math.Round(tmppixel.B * tmpk)));
                }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (oldbrtlist.Count == 0)
                return;
            oldbrtlist.RemoveAt(oldbrtlist.Count - 1);
            double resultk = 1;
            foreach (var s in oldbrtlist)
                resultk *= s;
            changebrt(resultk);
            pictureBox1.Image = pict;
            if (oldbrtlist.Count == 0)
            {
                notsaved = false;
                button2.Enabled = false;
            }
            trackBar1.Value = 0;
            panel1.Visible = false;
            panel2.Visible = false;
        }

        private void SaveFileMenuClick(object sender, EventArgs e)
        {
            if (pict == null)
                return;
            pict.Save(lastfile);
            oldbrtlist.Clear();
            oldpict = new Bitmap(pict);
            button2.Enabled = true;
            trackBar1.Value = 0;
            notsaved = false;
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            label6.Text = trackBar1.Value.ToString();
        }

        private void Print_Click(object sender, EventArgs e)
        {
            try
            {
                PrintPreviewDialog prev = new PrintPreviewDialog();
                PrintDocument Document = new PrintDocument();
                prev.Document = Document;
                Document.PrintPage += new PrintPageEventHandler(Document_PrintPage);
                DialogResult result = prev.ShowDialog();
                if (result == DialogResult.OK)
                {
                    Document.Print();
                    prev.Close();
                }
            }
            catch
            {
                return;
            }
        }
        void Document_PrintPage(object sender, PrintPageEventArgs e)
        {    
                    
            e.Graphics.DrawImage(pict, new Point(20, 20)); //Картинка на печать
        }

        private void Text_Click(object sender, EventArgs e)
        {
            try
            {
                Graphics g;
                oldpict = pict;
                pict = new Bitmap(oldpict);
                g = Graphics.FromImage(pict);
                Rectangle rect1 = new Rectangle(Convert.ToInt32(StartX.Text), Convert.ToInt32(StartY.Text), Convert.ToInt32(Width.Text), Convert.ToInt32(Height.Text));

                Font font = new Font(FontFamily.GenericSansSerif, 20, FontStyle.Regular);

                g.DrawString(TextBox.Text, font, new SolidBrush(Color.Black), rect1);

                pictureBox1.Image = pict;
                g.Save();
                g.Dispose();
                panel1.Visible = false;
                panel2.Visible = false;
                pict.Save(lastfile);
                oldbrtlist.Clear();
                oldpict = new Bitmap(pict);
                button2.Enabled = true;
                trackBar1.Value = 0;
                notsaved = false;
            }
            catch
            {
                return;
            }
        }

        private void BRB_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            panel2.Visible = false;
        }

        private void TB_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            panel2.Visible = true;
        }

     
    }
    }
