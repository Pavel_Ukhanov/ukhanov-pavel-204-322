﻿using System;
using System.Collections.Generic;

namespace labStackT
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack<int> x = new Stack<int>();
            x.Push(2);
            x.Push(123);
            x.Push(1243);

            Console.WriteLine(x.Peek());
            Console.WriteLine("---");

            int sum = 0;

            while (x.Count > 0)
            {
                int v = x.Pop();
                Console.WriteLine(v);
                sum += v;
            }

            Console.WriteLine("---");
            Console.WriteLine($"Sum = {sum}");

            try
            {
                Console.WriteLine(x.Pop());
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Что-то пошло не так : {ex.Message}");
            }
        }
    }
}
