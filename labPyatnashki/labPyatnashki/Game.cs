﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace labPyatnashki
{
    class Game
    {
        private Button[,] bu;
        private IFormPropertyManager _formPropertyManager;

        public int Cols { get; private set; } = 5;
        public int Rows { get; private set; } = 5;

        public Game(IFormPropertyManager formPropertyManager)
        {
            _formPropertyManager = formPropertyManager;

            CreateButtons();
            ResizeButtons();

            _formPropertyManager.ButtonRestart.Click += RestartGame;
            _formPropertyManager.ButtonAdd.Click += btnAdd_Click;
            _formPropertyManager.ButtonReduce.Click += btnReduce_Click;
        }

        public static void Shuffle(Button[,] array)
        {
            var random = new Random();

            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    int randi = random.Next(i);
                    int randj = random.Next(j);

                    var t = array[i, j];
                    array[i, j] = array[randi, randj];
                    array[randi, randj] = t;
                }
            }
        }

        private void Fm_MouseClick(object sender, MouseEventArgs e)
        {
            Button btn = (Button)sender;

            if (e.Button == MouseButtons.Left)
            {
                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Cols; j++)
                    {
                        try
                        {
                            if (btn.Text == bu[i, j].Text)
                            {
                                if (j < Cols - 1)
                                {
                                    if (bu[i, j + 1].Text == " ")
                                    {
                                        bu[i, j + 1].Text = btn.Text;
                                        btn.Text = " ";
                                    }
                                }
                                if (i > 0)
                                {
                                    if (bu[i - 1, j].Text == " ")
                                    {
                                        bu[i - 1, j].Text = btn.Text;
                                        btn.Text = " ";
                                    }
                                }
                                if (j > 0)
                                {
                                    if (bu[i, j - 1].Text == " ")
                                    {
                                        bu[i, j - 1].Text = btn.Text;
                                        btn.Text = " ";
                                    }
                                }
                                if (i < Rows)
                                {
                                    if (bu[i + 1, j].Text == " ")
                                    {
                                        bu[i + 1, j].Text = btn.Text;
                                        btn.Text = " ";
                                    }
                                }
                            }
                        }
                        catch { }
                    }
                }
                IsGameWin();
            }

            if (e.Button == MouseButtons.Right)
            {
                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Cols; j++)
                    {
                        if (btn.Text == bu[i, j].Text)
                        {
                            // ищем пустую ячейку справа
                            for (int p = j; p < Cols; p++)
                            {
                                if (bu[i, p].Text == " ")
                                {
                                    for (int back = p; back > j; back--)
                                    {
                                        bu[i, back].Text = bu[i, back - 1].Text;
                                    }
                                    bu[i, j].Text = " ";
                                }
                            }

                            //ищем пустую ячейку слева
                            for (int p = j; 0 <= p; p--)
                            {
                                if (bu[i, p].Text == " ")
                                {
                                    for (int back = p; back < j; back++)
                                    {
                                        bu[i, back].Text = bu[i, back + 1].Text;
                                    }
                                    bu[i, j].Text = " ";
                                }
                            }

                            //ищем пустую ячейку сверху
                            for (int p = i; 0 <= p; p--)
                            {
                                if (bu[p, j].Text == " ")
                                {
                                    for (int back = p; back < i; back++)
                                    {
                                        bu[back, j].Text = bu[back + 1, j].Text;
                                    }
                                    bu[i, j].Text = " ";
                                }
                            }

                            //ищем пустую ячейку снизу
                            for (int p = i; p < Rows; p++)
                            {
                                if (bu[p, j].Text == " ")
                                {
                                    for (int back = p; back > i; back--)
                                    {
                                        bu[back, j].Text = bu[back - 1, j].Text;
                                    }
                                    bu[i, j].Text = " ";
                                }
                            }
                        }
                    }
                }
            }
        }

        private void IsGameWin()
        {
            bool win = true;
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    if ((i * Cols + 1 + j) != (Rows * Cols))
                    {
                        if (bu[i, j].Text != (i * Cols + 1 + j).ToString())
                        {
                            win = false;
                        }
                    }
                }
            }
            if (win)
            {
                _formPropertyManager.Label.Visible = true;
                _formPropertyManager.Label.Text = "ПОБЕДА !!!";
                _formPropertyManager.GameDesk.Enabled = false;
            }
        }

        private void CreateButtons()
        {
            Random rand = new Random();
            bu = new Button[Rows, Cols];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    bu[i, j] = new Button();
                    bu[i, j].Text = (i * Cols + 1 + j).ToString(); // 1, 2, 4, 5...
                    bu[i, j].Font = new Font("Microsoft Sans Serif", 12F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(204)));
                    bu[i, j].KeyDown += Fm_KeyDown;
                    bu[i, j].MouseEnter += Fm_MouseEnter;
                    bu[i, j].MouseLeave += Fm_MouseLeave;
                    bu[i, j].MouseDown += Fm_MouseClick;

                    _formPropertyManager.GameDesk.Controls.Add(bu[i, j]);
                }
            }

            bu[Rows - 1, Cols - 1].Text = " ";
            Shuffle(bu);
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {

            switch (e.KeyCode)
            {
                case Keys.Q:
                    if (Cols != 3)
                    {
                        DeleteButtons();
                        if (Cols > 3)
                            Cols--;
                        if (Rows > 3)
                            Rows--;
                        CreateButtons();
                        ResizeButtons();
                    }
                    break;
                case Keys.W:
                    if (Cols != 6)
                    {
                        if (Cols < 6)
                            Cols++;
                        if (Rows < 6)
                            Rows++;
                        CreateButtons();
                        ResizeButtons();
                    }
                    break;
                case Keys.Z:
                    DeleteButtons();
                    break;
            }
        }

        private void DeleteButtons()
        {
            _formPropertyManager.GameDesk.Controls.Clear();
        }

        private void Fm_MouseLeave(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            btn.BackColor = default(Color);
            btn.ForeColor = Color.FromName("Black");
        }

        private void Fm_MouseEnter(object sender, EventArgs e)
        {
            //Random rnd = new Random();
            Button btn = (Button)sender;
            btn.BackColor = Color.FromName("DarkSlateGray");
            btn.ForeColor = Color.FromName("White");
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (Cols != 6)
            {
                DeleteButtons();
                if (Cols < 6)
                    Cols++;
                if (Rows < 6)
                    Rows++;
                CreateButtons();
                ResizeButtons();
            }
           
        }

        private void btnReduce_Click(object sender, EventArgs e)
        {
            if (Cols != 3)
            {
                DeleteButtons();
                if (Cols > 3)
                    Cols--;
                if (Rows > 3)
                    Rows--;
                CreateButtons();
                ResizeButtons();
            }
        }

        private void RestartGame(object sender, EventArgs e)
        {
            DeleteButtons();
            CreateButtons();
            ResizeButtons();

            _formPropertyManager.Label.Visible = false;
            _formPropertyManager.Label.Text = " ";
            _formPropertyManager.GameDesk.Enabled = true;
        }

        public void ResizeButtons()
        {
            int xCellWidth = _formPropertyManager.GameDesk.Width / Cols;
            int xCellHeight = _formPropertyManager.GameDesk.Height / Rows;
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    bu[i, j].Width = xCellWidth;
                    bu[i, j].Height = xCellHeight;
                    bu[i, j].Location = new Point(j * xCellWidth, i * xCellHeight); // X, Y
                }
        }

    }
}
