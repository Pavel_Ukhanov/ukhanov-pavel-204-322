﻿using System.Windows.Forms;

namespace labPyatnashki
{
    interface IFormPropertyManager
    {
        public Label Label { get; }

        public Button ButtonRestart { get; }

        public Button ButtonAdd { get; }

        public Button ButtonReduce { get; }

        public ListView GameDesk { get; }
    }
}
