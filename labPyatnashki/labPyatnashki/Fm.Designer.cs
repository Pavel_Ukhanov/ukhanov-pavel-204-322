﻿namespace labPyatnashki
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gameDesk = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.btnRestart = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnReduce = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // gameDesk
            // 
            this.gameDesk.HideSelection = false;
            this.gameDesk.Location = new System.Drawing.Point(158, 13);
            this.gameDesk.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gameDesk.Name = "gameDesk";
            this.gameDesk.Size = new System.Drawing.Size(896, 589);
            this.gameDesk.TabIndex = 0;
            this.gameDesk.UseCompatibleStateImageBehavior = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.label1.Font = new System.Drawing.Font("Showcard Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.Yellow;
            this.label1.Location = new System.Drawing.Point(416, 262);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(231, 74);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            this.label1.Visible = false;
            // 
            // btnRestart
            // 
            this.btnRestart.BackColor = System.Drawing.Color.SeaGreen;
            this.btnRestart.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.btnRestart.Location = new System.Drawing.Point(12, 248);
            this.btnRestart.MinimumSize = new System.Drawing.Size(140, 33);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(140, 39);
            this.btnRestart.TabIndex = 2;
            this.btnRestart.Text = "Новая игра";
            this.btnRestart.UseVisualStyleBackColor = false;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnAdd.Location = new System.Drawing.Point(12, 71);
            this.btnAdd.MinimumSize = new System.Drawing.Size(140, 46);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(140, 46);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "Увеличить игровое поле (+)\r\n";
            this.btnAdd.UseVisualStyleBackColor = false;
            // 
            // btnReduce
            // 
            this.btnReduce.BackColor = System.Drawing.Color.Silver;
            this.btnReduce.Location = new System.Drawing.Point(12, 153);
            this.btnReduce.MinimumSize = new System.Drawing.Size(140, 46);
            this.btnReduce.Name = "btnReduce";
            this.btnReduce.Size = new System.Drawing.Size(140, 73);
            this.btnReduce.TabIndex = 4;
            this.btnReduce.Text = "Уменьшить игровое поле (-)\r\n";
            this.btnReduce.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Silver;
            this.button1.Location = new System.Drawing.Point(12, 71);
            this.button1.MinimumSize = new System.Drawing.Size(140, 46);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(140, 76);
            this.button1.TabIndex = 3;
            this.button1.Text = "Увеличить игровое поле (+)\r\n";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1078, 644);
            this.Controls.Add(this.btnReduce);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnRestart);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gameDesk);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MinimumSize = new System.Drawing.Size(1080, 659);
            this.Name = "Fm";
            this.Text = "labPyatnashki";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView gameDesk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRestart;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnReduce;
        private System.Windows.Forms.Button button1;
    }
}

