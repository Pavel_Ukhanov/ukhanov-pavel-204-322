﻿using System.Windows.Forms;

namespace labPyatnashki
{
    public partial class Fm : Form, IFormPropertyManager
    {
        private readonly Game g;
        private Button[,] bu;
        public int Cols { get; private set; } = 5;
        public int Rows { get; private set; } = 5;
        public Fm()
        {
            InitializeComponent();

            g = new Game(this);
            KeyPreview = true;

            Resize += Fm_Resize;
        }

        private void Fm_Resize(object sender, System.EventArgs e)
        {
            g.ResizeButtons();
        }

        #region IFormPropertyManager
        public Label Label
        {
            get
            {
                return label1;
            }
        }

        public Button ButtonRestart
        {
            get
            {
                return btnRestart;
            }
        }

        public ListView GameDesk
        {
            get
            {
                return gameDesk;
            }
        }

        public Button ButtonAdd
        {
            get
            {
                return button1;
            }
        }

        public Button ButtonReduce
        {
            get
            {
                return btnReduce;
            }
        }
        #endregion
    }
}
