﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labTimer
{
    public partial class Fm : Form
    {
        Timer timerUp = new Timer();
        private DateTime startTimerUp;
        private DateTime pauseTimerUp;

        Timer timerDown = new Timer();
        private DateTime endTimerDown;

        private const int SEC_MAX = 12;
        public Fm()
        {
            InitializeComponent();

            timerUp.Interval = 100;
            timerUp.Tick += TmUp_Tick;
            buUp.Click += BuUp_Click;

            buUpPause.Click += BuUpPause_Click;
            pbUp.Maximum = SEC_MAX;
            pbUpMs.Maximum = SEC_MAX * 1000;

            timerDown.Interval = 100;
            timerDown.Tick += TmDown_Tick;
            buDown.Click += BuDown_Click;
        }
        private void TmUp_Tick(object sender, EventArgs e)
        {
            var x = DateTime.Now - startTimerUp;
            if (x.TotalSeconds > SEC_MAX)
            {
                timerUp.Stop();
                x = TimeSpan.FromSeconds(SEC_MAX);
            }
            buUp.Text = x.ToString(@"hh\:mm\:ss\.ff");
            pbUp.Value = (int) x.TotalSeconds;
            pbUpMs.Value = (int) x.TotalMilliseconds;
        }

        private void BuUp_Click(object sender, EventArgs e)
        {
            timerUp.Enabled = !timerUp.Enabled;
            startTimerUp = DateTime.Now;
        }

        private void BuUpPause_Click(object sender, EventArgs e)
        {
            if (timerUp.Enabled)
            {
                pauseTimerUp = DateTime.Now;
                timerUp.Stop();
            } else {
                startTimerUp += DateTime.Now - pauseTimerUp;
                timerUp.Start();
            }
        }
        private void TmDown_Tick(object sender, EventArgs e)
        {
            var x = endTimerDown - DateTime.Now;
            if (x.Ticks < 0)
            {
                timerDown.Stop();
                x = TimeSpan.Zero;
            }
            buDown.Text = x.ToString(@"hh\:mm\:ss\.ff");
        }

        private void BuDown_Click(object sender, EventArgs e)
        {
            timerDown.Enabled = !timerDown.Enabled;
            endTimerDown = DateTime.Now.AddSeconds(SEC_MAX);
        }
    }
}
