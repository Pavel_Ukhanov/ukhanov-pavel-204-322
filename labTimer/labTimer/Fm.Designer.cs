﻿
namespace labTimer
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buUp = new System.Windows.Forms.Button();
            this.buDown = new System.Windows.Forms.Button();
            this.buUpPause = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.pbUp = new System.Windows.Forms.ProgressBar();
            this.pbUpMs = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // buUp
            // 
            this.buUp.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buUp.Location = new System.Drawing.Point(12, 12);
            this.buUp.Name = "buUp";
            this.buUp.Size = new System.Drawing.Size(200, 90);
            this.buUp.TabIndex = 0;
            this.buUp.Text = "button1";
            this.buUp.UseVisualStyleBackColor = true;
            // 
            // buDown
            // 
            this.buDown.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buDown.Location = new System.Drawing.Point(12, 118);
            this.buDown.Name = "buDown";
            this.buDown.Size = new System.Drawing.Size(200, 90);
            this.buDown.TabIndex = 1;
            this.buDown.Text = "button2";
            this.buDown.UseVisualStyleBackColor = true;
            // 
            // buUpPause
            // 
            this.buUpPause.Location = new System.Drawing.Point(218, 12);
            this.buUpPause.Name = "buUpPause";
            this.buUpPause.Size = new System.Drawing.Size(90, 90);
            this.buUpPause.TabIndex = 2;
            this.buUpPause.Text = "button1";
            this.buUpPause.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(218, 118);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(90, 90);
            this.button2.TabIndex = 3;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // pbUp
            // 
            this.pbUp.Location = new System.Drawing.Point(314, 12);
            this.pbUp.Name = "pbUp";
            this.pbUp.Size = new System.Drawing.Size(166, 23);
            this.pbUp.TabIndex = 4;
            // 
            // pbUpMs
            // 
            this.pbUpMs.Location = new System.Drawing.Point(314, 41);
            this.pbUpMs.Name = "pbUpMs";
            this.pbUpMs.Size = new System.Drawing.Size(166, 23);
            this.pbUpMs.TabIndex = 5;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 220);
            this.Controls.Add(this.pbUpMs);
            this.Controls.Add(this.pbUp);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.buUpPause);
            this.Controls.Add(this.buDown);
            this.Controls.Add(this.buUp);
            this.Name = "Fm";
            this.Text = "labTimer";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buUp;
        private System.Windows.Forms.Button buDown;
        private System.Windows.Forms.Button buUpPause;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ProgressBar pbUp;
        private System.Windows.Forms.ProgressBar pbUpMs;
    }
}

