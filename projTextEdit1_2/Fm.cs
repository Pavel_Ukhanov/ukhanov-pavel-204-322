﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace textedit
{
    public partial class Fm : Form
    {
        string FileName = "";
        bool Save = false;

        public Fm()
        {
            InitializeComponent();
            ReadConfig();
        }

        private void создатьToolStripMenuItem_Click(object sender, EventArgs EA)
        {
            if (Save == false && textBox1.TextLength > 0)
            {
                DialogResult dialogResult = MessageBox.Show("Хотите сохранить файл перед закрытием?",
                "Редактор",
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Question);

                switch (dialogResult)
                {
                    case DialogResult.No:
                        {
                            FileName = "";
                            textBox1.Clear();
                            Save = false;
                            break;
                        }

                    case DialogResult.Yes:
                        {
                            сохранитьКакToolStripMenuItem_Click(sender, EA);
                            FileName = "";
                            textBox1.Clear();
                            Save = false;
                            break;
                        }

                    case DialogResult.Cancel:
                        {
                            break;
                        }

                    case DialogResult.Abort:
                        {
                            break;
                        }
                }
            }
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs EA)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileName = openFileDialog.FileName;
                textBox1.Text = File.ReadAllText(FileName);
                Save = false;
                this.Text = FileName;
            }
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs EA)
        {
            if (FileName.Length > 0)
            {
                File.WriteAllText(FileName, textBox1.Text);
                Save = true;
            }
            else
            {
                сохранитьКакToolStripMenuItem_Click(sender, EA);
            }
        }

        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text Files | *.txt| All Files | *.*";
            saveFileDialog.Title = "Сохранить как";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileName = saveFileDialog.FileName;
                File.WriteAllText(FileName, textBox1.Text);
                Save = true;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs EA)
        {
            Save = false;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs FCEA)
        {
            if (Save == false && textBox1.TextLength > 0)
            {
                DialogResult dialogResult = MessageBox.Show("Хотите сохранить файл перед выходом?",
                "Редактор",
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Question);

                switch (dialogResult)
                {
                    case DialogResult.No:
                        {
                            FCEA.Cancel = false;
                            break;
                        }

                    case DialogResult.Yes:
                        {
                            сохранитьКакToolStripMenuItem_Click(sender, FCEA);                           
                            FCEA.Cancel = false;
                            break;
                        }

                    case DialogResult.Cancel:
                        {
                            FCEA.Cancel = true;
                            break;
                        }

                    case DialogResult.Abort:
                        {
                            FCEA.Cancel = true;
                            break;
                        }
                }
            }
            SaveConfig();
        }

        private void шрифтToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FontDialog fontDialog = new FontDialog();

            if (fontDialog.ShowDialog() == DialogResult.OK)
            {
                textBox1.Font = fontDialog.Font;
            }
        }

        private void цветToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();

            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                textBox1.ForeColor = colorDialog.Color;
            }
        }

        public void ReadConfig()
        {
            textBox1.Font = Properties.Settings.Default.font;
            textBox1.ForeColor = Properties.Settings.Default.color;
        }

        public void SaveConfig()
        {
            Properties.Settings.Default.font = textBox1.Font;
            Properties.Settings.Default.color = textBox1.ForeColor;
            Properties.Settings.Default.Save();
        }

        private void оРазработчикеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("204-322 Уханов Павел", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}