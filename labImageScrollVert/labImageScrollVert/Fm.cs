﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageScrollVert
{
    
    public partial class Fm : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        private Bitmap imgBG;
        private Bitmap car;
        private int deltaY = 0;
        private int deltaX = 0;
        private int stackImgs = 0;
        private Point startPoint;

        private int speed;
        private readonly int defaultSpeed;

        public Fm()
        {
            InitializeComponent();

            defaultSpeed = 20;
            speed = defaultSpeed;
            imgBG = Properties.Resources.fon;
            car = Properties.Resources.car;
            
            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);
       
            DoubleBuffered = true;
            Paint += (s, e) => { UpdateBG(); e.Graphics.DrawImage(b, 0, 0); };
            MouseDown += (s, e) => startPoint = e.Location;
            MouseMove += Fm_MouseMove;
            KeyDown += Fm_KeyDown;
            KeyUp += Fm_KeyUp;

        }
        private void Fm_KeyUp(object sender, KeyEventArgs e)
        {
           
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    UpdateDeltaY(speed);
                    break;
                case Keys.Down:
                    UpdateDeltaY(-speed);
                    break;
                case Keys.Left:
                    UpdateDeltaY(speed);
                    UpdateDeltaX(-speed/2);
                    break;
                case Keys.Right:
                    UpdateDeltaY(speed);
                    UpdateDeltaX(speed/2);
                    break;
            }
            Invalidate();
        }

        private void Fm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                UpdateDeltaY(e.Y - startPoint.Y);
                startPoint = e.Location;
                Invalidate();
            }
        }

        private void UpdateDeltaY(int v)
        {
            Text = $"{Application.ProductName} : deltaY={deltaY}, v={v}, deltaX = {deltaX}";
            deltaY += v;

            if (deltaY > 0)
                deltaY -= imgBG.Height;
            else
                if (deltaY < -imgBG.Height)
                deltaY += imgBG.Height;
        }

        private void UpdateDeltaX(int v)
        {
            deltaX += v;
        }

        private void UpdateBG()
        {
            stackImgs = (ActiveForm.Height / imgBG.Height) + 1;
            

            if (stackImgs == 1)
            {
                stackImgs++;
            }

            for (int i = 0; i < stackImgs; i++)
            {
                g.DrawImage(imgBG, 0, deltaY + i * imgBG.Height);
            }

            g.DrawImage(car, deltaX + car.Height, 200);
        }
    }
}
