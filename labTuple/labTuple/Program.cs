﻿using System;

namespace labTuple
{
    class Program
    {
        static void Main(string[] args)
        {
            var x1 = (0, 1); 
            Console.WriteLine(x1.Item1);
            Console.WriteLine(x1.Item2);
            Console.WriteLine();
         
            (int min, int max) x2 = (2, 3);
            Console.WriteLine(x2.min);
            Console.WriteLine(x2.max);
            Console.WriteLine();
          
            var x3 = (min: 4, max: 5);
            Console.WriteLine(x3.min);
            Console.WriteLine(x3.max);
            Console.WriteLine();
            
            var (min, max) = (6, 7);
            Console.WriteLine(min);
            Console.WriteLine(max);
            Console.WriteLine();
           
            var x5 = GetX5();
            Console.WriteLine(x5.Item1);
            Console.WriteLine(x5.Item2);
            Console.WriteLine();
            
            var x6 = GetX6();
            Console.WriteLine(x6.min);
            Console.WriteLine(x6.max);
            Console.WriteLine();
            
            var x7 = GetX7((3, 10), 14);
            Console.WriteLine(x7.min);
            Console.WriteLine(x7.max);
            Console.WriteLine();
        }

        private static (int, int) GetX5() => (8, 9);
        private static (int min, int max) GetX6() => (10, 11);
        private static (int min, int max) GetX7((int, int) p, int v) => (p.Item1 + p.Item2, v);
    }
}
