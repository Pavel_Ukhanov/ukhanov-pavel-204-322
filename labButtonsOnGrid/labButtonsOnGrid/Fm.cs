﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labButtonsOnGrid
{
    public partial class Fm : Form
    {
        private Button[,] bu;

        public int Cols { get; private set; } = 4;
        public int Rows { get; private set; } = 3;
        public Fm()
        {
            InitializeComponent();

            CreateButtons();
            ResizeButtons();
            this.Resize += (s, e) => ResizeButtons();
            this.KeyDown += Fm_KeyDown;
        }

        private void ResizeButtons()
        {
            int xCellWidth = this.ClientSize.Width / Cols;
            int xCellHeight = this.ClientSize.Height / Rows;
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    bu[i, j].Width = xCellWidth;
                    bu[i, j].Height = xCellHeight;
                    bu[i, j].Location = new Point(j * xCellWidth, i * xCellHeight); // X, Y
                }
        }
        private void DeleteButtons()
        {
            this.Controls.Clear();
        }
        private void CreateButtons()
        {
            bu = new Button[Rows, Cols];
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    bu[i, j] = new Button();
                    bu[i, j].Text = (i * Cols + 1 + j).ToString(); // 1, 2, 4, 5...
                    bu[i, j].KeyDown += Fm_KeyDown;
                    bu[i, j].MouseEnter += Fm_MouseEnter;
                    bu[i, j].MouseLeave += Fm_MouseLeave; ;
                    this.Controls.Add(bu[i, j]);

                }
        }

        private void Fm_MouseLeave(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            btn.BackColor = default(Color);
        }

        private void Fm_MouseEnter(object sender, EventArgs e)
        {
            Random rnd = new Random();
            Button btn = (Button)sender;
            btn.BackColor = Color.FromArgb(rnd.Next(0, 256), rnd.Next(0, 256), rnd.Next(0, 256));
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            DeleteButtons();

            switch (e.KeyCode)
            {
                case Keys.Q:
                    if (Cols > 1)
                        Cols--;
                    break;
                case Keys.W:
                    if (Cols < 10)
                        Cols++;
                    break;
                case Keys.A:
                    if (Rows > 1)
                        Rows--;
                    break;
                case Keys.S:
                    if (Rows < 10)
                        Rows++;
                    break;
                case Keys.Z:
                    DeleteButtons();
                    break;
            }
            CreateButtons();
            ResizeButtons();
        }

    }
}
