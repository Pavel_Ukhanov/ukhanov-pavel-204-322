﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabFileExplorer
{
    public partial class Fm : Form
    {
        public string path { get; private set; }
        public string item { get; private set; }

        public Fm()
        {
            InitializeComponent();

            path = Directory.GetCurrentDirectory();

            buttonUp.Click += (s, e) => LoadDir(Directory.GetParent(path).ToString());
            dir.KeyDown += EdDir_KeyDown;
            buttonSelect.Click += BuDirSelect_Click;

            miViewLargeIcon.Click += (s, e) => l.View = View.LargeIcon;
            miViewTile.Click += (s, e) => l.View = View.Tile;
            miViewSmallIcon.Click += (s, e) => l.View = View.SmallIcon;
            miViewList.Click += (s, e) => l.View = View.List;
            miViewDetails.Click += (s, e) => l.View = View.Details;

            l.ItemSelectionChanged += (s, e) => item = Path.Combine(path, e.Item.Text);
            l.DoubleClick += (s, e) => LoadDir(item);

            l.Columns.Add("Имя", 400);
            l.Columns.Add("Дата изменения", 150);
            l.Columns.Add("Тип", 100);
            l.Columns.Add("Размер", 150);

            LoadDir(path);

            this.Text += " : Drivers = " + string.Join(" ", Directory.GetLogicalDrives());
        }

        private void BuDirSelect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
                LoadDir(dialog.SelectedPath);
        }
        
        private void EdDir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LoadDir(dir.Text);
            }
        }

        private void LoadDir(string newPath)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(newPath);

            l.BeginUpdate();
            l.Items.Clear();

            foreach (var item in directoryInfo.GetDirectories())
            {
                l.Items.Add(new ListViewItem( new string[] { item.Name, item.LastWriteTime.ToString(), "Папка", "" }, 0));
            }

            foreach (var item in directoryInfo.GetFiles())
            {
                l.Items.Add(new ListViewItem(new string[] { item.Name, item.LastWriteTime.ToString(), "Файл", item.Length.ToString() + " байт" }, 1));
            }

            l.EndUpdate();
            path = newPath;
            dir.Text = newPath;
        }
    }
}
