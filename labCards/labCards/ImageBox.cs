﻿using System;
using System.Drawing;

namespace labCards
{
    internal class ImageBox
    {
        private Bitmap[] images;

        public int Rows { get; }
        public int Cols { get; }
        public int Count { get; }
        public int Width { get; set; }
        public int Height { get; set; }

        public ImageBox(Bitmap image, int rows, int cols) : this(image, rows, cols, rows * cols) { }

        public ImageBox(Bitmap image, int rows, int cols, int count)
        {
            Rows = rows;
            Cols = cols;
            Count = count;

            LoadImages(image);
        }

        public Bitmap this[int index] { get { return images[index];  } } // Индексатор 

        private void LoadImages(Bitmap image)
        {
            images = new Bitmap[Count];
            Width = image.Width / Cols;
            Height = image.Height / Rows;
            var n = 0;

            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols && n < Count; j++, n++)
                {
                    images[n] = new Bitmap(Width, Height);

                    var g = Graphics.FromImage(images[n]);
                    g.DrawImage(image, 0, 0, new Rectangle(j * Width, i * Height, Width, Height), GraphicsUnit.Pixel);
                    g.Dispose();
                }
        }
    }
}