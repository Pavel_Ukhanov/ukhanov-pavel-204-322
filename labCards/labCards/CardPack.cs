﻿using System;
using System.Linq;

namespace labCards
{
    internal class CardPack
    {
        private int[] arr;
        private Random rnd = new Random();
        public int Count { get; }

        public CardPack(int count)
        {
            Count = count;
            CardSort();
        }

        public int this[int index] { get { return arr[index]; } }

        public void CardSort()
        {
            arr = Enumerable.Range(0, Count).ToArray();
        }

        public void CardRandom()
        {
            arr = Enumerable.Range(0, Count).OrderBy(_ => rnd.Next()).ToArray();
        }
    }
}