﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labCards
{
    public partial class Fm : Form
    {
        private const int cardCount = 5;
        
        private readonly Bitmap b;
        private readonly Bitmap s;
        private readonly Graphics g;
        private readonly Graphics f;
        private readonly ImageBox iBox; // коллекция
        private readonly CardPack cPack; // колода

        private Point startPoint;

        public Fm()
        {
            InitializeComponent();

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);

            s = new Bitmap(pictureBox.Width, pictureBox.Height);
            f = Graphics.FromImage(s);

            iBox = new ImageBox(Properties.Resources.allcards, 4, 13);

            cPack = new CardPack(iBox.Count);

            this.DoubleBuffered = true;
            this.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);
            this.KeyDown += Fm_KeyDown;

            pictureBox.MouseDown += PictureBox_MouseDown;
            pictureBox.MouseMove += PictureBox_MouseMove;
            pictureBox.MouseWheel += PictureBox_MouseWheel;
        }

        private void PictureBox_MouseWheel(object sender, MouseEventArgs e)
        {
            int xDelta = e.Delta > 0 ? -2 : 2;
            if (sender is Control x)
            {
                x.Width += xDelta * 2;
                x.Height += xDelta * 2;
                x.Location = new Point(x.Location.X + xDelta * -1, x.Location.Y + xDelta * -1);
            }
        }

        private void PictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point xPoint = new Point(Cursor.Position.X - startPoint.X, Cursor.Position.Y - startPoint.Y);
                if (sender is Control x)
                    x.Location = PointToClient(xPoint);
            }
        }

        private void PictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            startPoint = e.Location;

            if (sender is Control x)
                x.BringToFront();
        }


        private void VisibleFanCard()
        {
            pictureBox.Visible = true;
            pictureBox.Location = new Point(0, 0);
            pictureBox.Width = Screen.PrimaryScreen.Bounds.Width;
            pictureBox.Height = Screen.PrimaryScreen.Bounds.Height;

            f.TranslateTransform(this.ClientSize.Width / 2, this.ClientSize.Height / 2);
            f.RotateTransform(-90);

            for (int i = 0; i < cardCount; i++)
            {
                f.RotateTransform(180 / cardCount);
                f.DrawImage(iBox[cPack[i]], -iBox[i].Width / 2, -iBox[i].Height);
            }

            f.ResetTransform();
            pictureBox.BackgroundImage = null;
            pictureBox.BackgroundImage = s;
        }
        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            g.Clear(SystemColors.Control);
            f.Clear(SystemColors.Control);

            switch (e.KeyCode)
            {
                case Keys.F3:
                    cPack.CardSort();
                    VisibleFanCard();
                    break;

                case Keys.F4:
                    cPack.CardRandom();
                    VisibleFanCard();
                    break;
                
                case Keys.F5: 
                    cPack.CardSort();
                    VisibleCard();
                    break;

                case Keys.F6:
                    cPack.CardRandom();
                    VisibleCard();
                    break;
            }
        }

        private void VisibleCard()
        {
            pictureBox.Visible = false;

            Random rnd = new Random();

            for (int i = 0; i < cardCount; i++)
            {
                g.DrawImage(iBox[cPack[i]],
                            rnd.Next(this.ClientSize.Width - iBox.Width), 
                            rnd.Next(this.ClientSize.Height - iBox.Height));
            }

            this.Invalidate();
        }
    }
}
