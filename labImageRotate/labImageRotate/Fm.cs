﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageRotate
{
    public partial class Fm : Form
    {
        private readonly Bitmap imgHero;

        public Fm()
        {
            InitializeComponent();

            imgHero = new Bitmap(Properties.Resources.Hero);

            trackBar1.ValueChanged += (s, e) => pictureBox1.Invalidate();
            pictureBox1.Paint += PictureBox1_Paint;

            button1.Click += (s, e) => { imgHero.RotateFlip(RotateFlipType.RotateNoneFlipX); }; pictureBox1.Invalidate();
            button2.Click += (s, e) => { imgHero.RotateFlip(RotateFlipType.RotateNoneFlipY); }; pictureBox1.Invalidate();

            //
            //imgHero.RotateFlip(____);
            //pictureBox1.Image.RotateFlip(_______);
        }

        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.TranslateTransform(imgHero.Width / 2, imgHero.Height / 2);
            e.Graphics.RotateTransform(trackBar1.Value);
            e.Graphics.DrawImage(imgHero, -imgHero.Width / 2, -imgHero.Height / 2);

            e.Graphics.RotateTransform(trackBar1.Value);
            e.Graphics.TranslateTransform(-imgHero.Width / 2, -imgHero.Height / 2);
            e.Graphics.DrawImage(imgHero, 150, 150);
        }
    }
}
