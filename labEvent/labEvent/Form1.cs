﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labEvent
{
    public partial class Fm : Form
    {
        public Fm()
        {
            InitializeComponent();

            button2.Click += Button2_Click;
            button3.Click += delegate  // делегат вызывающий анонимный метод
            {
                MessageBox.Show("Способ 3");
            };
            //button3.Click += Button2_Click;
            button4.Click += (s, e) => MessageBox.Show("Способ 4"); // обработчик события через лямбда-выражение
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Способ 2");
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Способ 1");
        }
    }
}
