﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labImageSelectionGrid
{
    public partial class Fm : Form
    {

        private readonly Bitmap b;
        private readonly Graphics g;

        private (int x, int y, int height, int width) positionBitmap;
        private Point startPoint;

        private int cellWidth;
        private int cellHeight;
        private int curRow;
        private int curCol;
        private int sizeFont = 15;

        private (int row, int col) cellBegin;
        private (int row, int col) cellEnd;
        private int xDelta;
        private int yDelta;

        private const int MIN_ROWS = 2;
        private const int MAX_ROWS = 10;
        private const int MIN_COLS = 2;
        private const int MAX_COLS = 15;
        public int Cols { get; private set; } = 5;
        public int Rows { get; private set; } = 5;

        public Fm()
        {
            InitializeComponent();

            positionBitmap.x = 0;
            positionBitmap.y = 0;
            positionBitmap.width = Screen.PrimaryScreen.Bounds.Width;
            positionBitmap.height = Screen.PrimaryScreen.Bounds.Height;

            b = new Bitmap(positionBitmap.width, positionBitmap.height);
            g = Graphics.FromImage(b);

            this.DoubleBuffered = true;
            this.Paint += (s, e) => e.Graphics.DrawImage(b, positionBitmap.x, positionBitmap.y);
            this.Resize += (s, e) => ResizeCells();
            
            this.MouseDown += Fm_MouseDown;
            this.MouseUp += Fm_MouseUp;

            this.MouseMove += Fm_MouseMove;
            this.MouseWheel += Fm_MouseWheel;

            this.KeyDown += Fm_KeyDown;

            this.Text = $"{Application.ProductName} : F5/F6 - Rows, F7/F8 - Cols, MouseWeel";
            ResizeCells();
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F5:
                    if (Rows > MIN_ROWS)
                    {
                        Rows--;
                        ResizeCells();
                    }
                    break;

                case Keys.F6:
                    if (Rows < MAX_ROWS)
                    {
                        Rows++;
                        ResizeCells();
                    }
                    break;

                case Keys.F7:
                    if (Cols > MIN_COLS)
                    {
                        Cols--;
                        if (sizeFont < 26)
                            sizeFont += 2;
                        ResizeCells();
                    }
                    break;

                case Keys.F8:
                    if (Cols < MAX_COLS)
                    {
                        Cols++;
                        if (sizeFont > 13)
                            sizeFont -= 3;
                        ResizeCells();
                    }
                    break;
                
                default:
                    break;
            }
        }

        private void Fm_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                cellBegin = cellEnd = (curRow, curCol);
                DrawCells();
            }

            if (e.Button == MouseButtons.Right)
            {
                startPoint = e.Location;
            }
        }
        
        private void Fm_MouseUp(object sender, MouseEventArgs e)
        {
            positionBitmap.x = xDelta;
            positionBitmap.y = yDelta;
        }

        private void Fm_MouseMove(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < Rows; curRow = i, i++)
                if (i * cellHeight > e.Y) break;

            for (int i = 0; i < Cols; curCol = i, i++)
                if (i * cellWidth > e.X) break;

            if (e.Button == MouseButtons.Left)
                cellEnd = (curRow, curCol);

            this.Text = $"{cellBegin.row} : {cellBegin.col} - {cellEnd.row} : {cellEnd.col}";
            
            if (e.Button == MouseButtons.Right)
            {
                xDelta = positionBitmap.x - (startPoint.X - e.Location.X);
                yDelta = positionBitmap.y - (startPoint.Y - e.Location.Y);

                this.Paint += (s, e) =>
                {
                    e.Graphics.Clear(DefaultBackColor);
                    e.Graphics.DrawImage(b, xDelta, yDelta, positionBitmap.width, positionBitmap.height);
                };
            }

            DrawCells();
        }

        private void Fm_MouseWheel(object sender, MouseEventArgs e)
        {
            int delta = e.Delta > 0 ? -1 : 1;
            int coef = 10;

            if (positionBitmap.width <= Screen.PrimaryScreen.Bounds.Width || positionBitmap.height <= Screen.PrimaryScreen.Bounds.Height)
            {
                if (positionBitmap.width >= 200)
                {
                    positionBitmap.height += delta * coef;
                    positionBitmap.width += delta * coef;
                } else {
                    positionBitmap.height -= delta * coef;
                    positionBitmap.width -= delta * coef;
                }
            } else {
                positionBitmap.height = Screen.PrimaryScreen.Bounds.Height;
                positionBitmap.width = Screen.PrimaryScreen.Bounds.Width;
            }

            this.Paint += (s, e) =>
            {
                e.Graphics.Clear(DefaultBackColor);
                e.Graphics.DrawImage(b, positionBitmap.x, positionBitmap.y, positionBitmap.width, positionBitmap.height);
            };

            DrawCells();
        }

        private void ResizeCells()
        {
            cellWidth = this.ClientSize.Width / Cols;
            cellHeight = this.ClientSize.Height / Rows;

            DrawCells();
        }

        private void DrawCells()
        {
            g.Clear(DefaultBackColor);

            if (cellEnd.col <= cellBegin.col && cellEnd.row <= cellBegin.row)
            {
                g.FillRectangle(new SolidBrush(Color.LightBlue),
                           cellEnd.col * cellWidth,
                           cellEnd.row * cellHeight,
                           (cellBegin.col - cellEnd.col + 1) * cellWidth,
                           (cellBegin.row - cellEnd.row + 1) * cellHeight);
            }
                else if (cellEnd.col <= cellBegin.col && cellEnd.row >= cellBegin.row)
            {
                g.FillRectangle(new SolidBrush(Color.LightBlue),
                           cellEnd.col * cellWidth,
                           cellBegin.row * cellHeight,
                           (cellBegin.col - cellEnd.col + 1) * cellWidth,
                           (cellEnd.row - cellBegin.row + 1) * cellHeight);
            }
                else if (cellEnd.col >= cellBegin.col && cellEnd.row <= cellBegin.row)
            {
                g.FillRectangle(new SolidBrush(Color.LightBlue),
                           cellBegin.col * cellWidth,
                           cellEnd.row * cellHeight,
                           (cellEnd.col - cellBegin.col + 1) * cellWidth,
                           (cellBegin.row - cellEnd.row + 1) * cellHeight);
            }
                else
            {
                g.FillRectangle(new SolidBrush(Color.LightBlue),
                            cellBegin.col * cellWidth,
                            cellBegin.row * cellHeight,
                            (cellEnd.col - cellBegin.col + 1) * cellWidth,
                            (cellEnd.row - cellBegin.row + 1) * cellHeight);
            }

            for (int i = 0; i <= Rows; i++)
                g.DrawLine(new Pen(Color.Green, 1), 
                           0, 
                           i * cellHeight, 
                           Cols * cellWidth, 
                           i * cellHeight);

            for (int i = 0; i <= Cols; i++)
                g.DrawLine(new Pen(Color.Green, 1), 
                           i * cellWidth, 
                           0, 
                           i * cellWidth, 
                           Rows * cellHeight);

            g.DrawRectangle(new Pen(Color.Red, 5), 
                            curCol * cellWidth, 
                            curRow * cellHeight, 
                            cellWidth, 
                            cellHeight);

            g.DrawString($"[{curRow}:{curCol}]", new Font("", sizeFont), new SolidBrush(Color.Black), curCol * cellWidth, curRow * cellHeight);

            this.Invalidate();
        }
    }
}
