﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPaint
{
    public partial class Fm : Form
    {
        private readonly Bitmap b;
        private readonly Graphics g;
        private Point startPoint;
        private Pen myPen;
        private Pen penForRectangle;

        private static int X = 0;
        private static int Y = 0;

        private static int X1 = 0;
        private static int Y1 = 0;
        public Fm()
        {
            InitializeComponent();

            b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            g = Graphics.FromImage(b);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            myPen = new Pen(pxColor1.BackColor, 10);
            penForRectangle = new Pen(Color.Blue);

            myPen.StartCap = myPen.EndCap = System.Drawing.Drawing2D.LineCap.Round;

            pxImage.MouseDown += PxImage_MouseDown;
           
            pxImage.MouseMove += PxImage_MouseMove;
            pxImage.Paint += (s, e) => e.Graphics.DrawImage(b, 0, 0);

            trPenWidth.Value = Convert.ToInt32(myPen.Width);
            trPenWidth.ValueChanged += (s, e) => myPen.Width = trPenWidth.Value;

            pxColor1.Click += PxColor1_Click;
            pxColor2.Click += PxColor2_Click;
            pxColor3.Click += PxColor3_Click;
            pxColor4.Click += PxColor4_Click;

            btnEraser.Click += BtnEraser_Click;

            buImageClear.Click += delegate
            {
                g.Clear(SystemColors.Control);
                pxImage.Invalidate();
            };

            buImageSave.Click += BuImageSave_Click;
            buImageLoad.Click += BuImageLoad_Click;
            buAddRandomStar.Click += buAddRandomStar_Click;
            buColorDialog.Click += BuColorDialog_Click;
        }

        private void PxImage_MouseDown(object sender, MouseEventArgs e)
        {
            startPoint = e.Location;

            X = e.X;
            Y = e.Y;
            X1 = e.X;
            Y1 = e.Y;
        }

        private void BtnEraser_Click(object sender, EventArgs e)
        {
            myPen.Color = SystemColors.Control;
        }
        private void CreateRotateRectangle()
        {
            Graphics graphics = pxImage.CreateGraphics();
            graphics.DrawRectangle(penForRectangle, X, Y, X1 - X, Y1 - Y);
            graphics.Dispose();
            pxImage.Invalidate();
        }
        private void PxColor1_Click(object sender, EventArgs e)
        {
            myPen.Color = pxColor1.BackColor;
            pictureBox1.BackColor = pxColor1.BackColor;
        }

        private void PxColor2_Click(object sender, EventArgs e)
        {
            myPen.Color = pxColor2.BackColor;
            pictureBox1.BackColor = pxColor2.BackColor;
        }

        private void PxColor3_Click(object sender, EventArgs e)
        {
            myPen.Color = pxColor3.BackColor;
            pictureBox1.BackColor = pxColor3.BackColor;
        }

        private void PxColor4_Click(object sender, EventArgs e)
        {
            myPen.Color = pxColor4.BackColor;
            pictureBox1.BackColor = pxColor4.BackColor;
        }

        private void BuColorDialog_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.Cancel)
                return;

            myPen.Color = colorDialog1.Color;
            pictureBox1.BackColor = colorDialog1.Color;
        }

        private void BuImageLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                g.Clear(SystemColors.Control);
                g.DrawImage(Bitmap.FromFile(dialog.FileName), 0, 0);
                pxImage.Invalidate();
            }
        }

        private void BuImageSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Image Files(*.JPG)|*.JPG";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                b.Save(dialog.FileName);
            }
        }

        private void buAddRandomStar_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            for (int i = 0; i < 100; i++)
            {
                g.FillEllipse(new SolidBrush(Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256))), 
                    rnd.Next(b.Width), 
                    rnd.Next(b.Height), 
                    rnd.Next(1, 10),
                    rnd.Next(1, 10)
                    );
            }
            pxImage.Invalidate();
        }

        private void PxImage_MouseMove(object sender, MouseEventArgs e)
        {
            X1 = e.X;
            Y1 = e.Y;

            if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right)
            {
                if (checkBox1.Checked)
                {
                    CreateRotateRectangle();
                    return;
                }

                g.DrawLine(myPen, startPoint, e.Location);
                startPoint = e.Location;
                pxImage.Invalidate();
            }
        }
    }
}
