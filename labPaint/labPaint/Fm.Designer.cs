﻿namespace labPaint
{
    partial class Fm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.btnEraser = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buColorDialog = new System.Windows.Forms.Button();
            this.buAddRandomStar = new System.Windows.Forms.Button();
            this.buImageLoad = new System.Windows.Forms.Button();
            this.buImageSave = new System.Windows.Forms.Button();
            this.buImageClear = new System.Windows.Forms.Button();
            this.trPenWidth = new System.Windows.Forms.TrackBar();
            this.pxColor4 = new System.Windows.Forms.PictureBox();
            this.pxColor3 = new System.Windows.Forms.PictureBox();
            this.pxColor2 = new System.Windows.Forms.PictureBox();
            this.pxColor1 = new System.Windows.Forms.PictureBox();
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trPenWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.btnEraser);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.buColorDialog);
            this.panel1.Controls.Add(this.buAddRandomStar);
            this.panel1.Controls.Add(this.buImageLoad);
            this.panel1.Controls.Add(this.buImageSave);
            this.panel1.Controls.Add(this.buImageClear);
            this.panel1.Controls.Add(this.trPenWidth);
            this.panel1.Controls.Add(this.pxColor4);
            this.panel1.Controls.Add(this.pxColor3);
            this.panel1.Controls.Add(this.pxColor2);
            this.panel1.Controls.Add(this.pxColor1);
            this.panel1.Location = new System.Drawing.Point(1, 3);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(234, 561);
            this.panel1.TabIndex = 0;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(29, 507);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkBox1.Size = new System.Drawing.Size(142, 24);
            this.checkBox1.TabIndex = 7;
            this.checkBox1.Text = "Прямоугольник";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // btnEraser
            // 
            this.btnEraser.Location = new System.Drawing.Point(27, 409);
            this.btnEraser.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEraser.Name = "btnEraser";
            this.btnEraser.Size = new System.Drawing.Size(71, 29);
            this.btnEraser.TabIndex = 6;
            this.btnEraser.Text = "Ластик";
            this.btnEraser.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Blue;
            this.pictureBox1.Location = new System.Drawing.Point(154, 459);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(63, 24);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 461);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Выбраный цвет";
            // 
            // buColorDialog
            // 
            this.buColorDialog.Location = new System.Drawing.Point(11, 9);
            this.buColorDialog.Name = "buColorDialog";
            this.buColorDialog.Size = new System.Drawing.Size(205, 29);
            this.buColorDialog.TabIndex = 4;
            this.buColorDialog.Text = "Палитра цветов";
            this.buColorDialog.UseVisualStyleBackColor = true;
            // 
            // buAddRandomStar
            // 
            this.buAddRandomStar.Location = new System.Drawing.Point(29, 371);
            this.buAddRandomStar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buAddRandomStar.Name = "buAddRandomStar";
            this.buAddRandomStar.Size = new System.Drawing.Size(189, 31);
            this.buAddRandomStar.TabIndex = 3;
            this.buAddRandomStar.Text = "Add random star";
            this.buAddRandomStar.UseVisualStyleBackColor = true;
            // 
            // buImageLoad
            // 
            this.buImageLoad.Location = new System.Drawing.Point(27, 316);
            this.buImageLoad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buImageLoad.Name = "buImageLoad";
            this.buImageLoad.Size = new System.Drawing.Size(189, 31);
            this.buImageLoad.TabIndex = 3;
            this.buImageLoad.Text = "Load from file";
            this.buImageLoad.UseVisualStyleBackColor = true;
            // 
            // buImageSave
            // 
            this.buImageSave.Location = new System.Drawing.Point(29, 264);
            this.buImageSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buImageSave.Name = "buImageSave";
            this.buImageSave.Size = new System.Drawing.Size(189, 31);
            this.buImageSave.TabIndex = 3;
            this.buImageSave.Text = "Save to file";
            this.buImageSave.UseVisualStyleBackColor = true;
            // 
            // buImageClear
            // 
            this.buImageClear.Location = new System.Drawing.Point(27, 208);
            this.buImageClear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buImageClear.Name = "buImageClear";
            this.buImageClear.Size = new System.Drawing.Size(189, 31);
            this.buImageClear.TabIndex = 3;
            this.buImageClear.Text = "Clear";
            this.buImageClear.UseVisualStyleBackColor = true;
            // 
            // trPenWidth
            // 
            this.trPenWidth.Location = new System.Drawing.Point(17, 139);
            this.trPenWidth.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.trPenWidth.Minimum = 1;
            this.trPenWidth.Name = "trPenWidth";
            this.trPenWidth.Size = new System.Drawing.Size(199, 56);
            this.trPenWidth.TabIndex = 2;
            this.trPenWidth.Value = 1;
            // 
            // pxColor4
            // 
            this.pxColor4.BackColor = System.Drawing.Color.Yellow;
            this.pxColor4.Location = new System.Drawing.Point(171, 63);
            this.pxColor4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pxColor4.Name = "pxColor4";
            this.pxColor4.Size = new System.Drawing.Size(45, 48);
            this.pxColor4.TabIndex = 0;
            this.pxColor4.TabStop = false;
            // 
            // pxColor3
            // 
            this.pxColor3.BackColor = System.Drawing.Color.Red;
            this.pxColor3.Location = new System.Drawing.Point(120, 63);
            this.pxColor3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pxColor3.Name = "pxColor3";
            this.pxColor3.Size = new System.Drawing.Size(45, 48);
            this.pxColor3.TabIndex = 0;
            this.pxColor3.TabStop = false;
            // 
            // pxColor2
            // 
            this.pxColor2.BackColor = System.Drawing.Color.Lime;
            this.pxColor2.Location = new System.Drawing.Point(69, 63);
            this.pxColor2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pxColor2.Name = "pxColor2";
            this.pxColor2.Size = new System.Drawing.Size(45, 48);
            this.pxColor2.TabIndex = 0;
            this.pxColor2.TabStop = false;
            // 
            // pxColor1
            // 
            this.pxColor1.BackColor = System.Drawing.Color.Blue;
            this.pxColor1.Location = new System.Drawing.Point(17, 63);
            this.pxColor1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pxColor1.Name = "pxColor1";
            this.pxColor1.Size = new System.Drawing.Size(45, 48);
            this.pxColor1.TabIndex = 0;
            this.pxColor1.TabStop = false;
            // 
            // pxImage
            // 
            this.pxImage.Location = new System.Drawing.Point(242, 3);
            this.pxImage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pxImage.Name = "pxImage";
            this.pxImage.Size = new System.Drawing.Size(671, 561);
            this.pxImage.TabIndex = 1;
            this.pxImage.TabStop = false;
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(918, 616);
            this.Controls.Add(this.pxImage);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Fm";
            this.Text = "labPaint";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trPenWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pxColor4;
        private System.Windows.Forms.PictureBox pxColor3;
        private System.Windows.Forms.PictureBox pxColor2;
        private System.Windows.Forms.PictureBox pxColor1;
        private System.Windows.Forms.PictureBox pxImage;
        private System.Windows.Forms.TrackBar trPenWidth;
        private System.Windows.Forms.Button buAddRandomStar;
        private System.Windows.Forms.Button buImageLoad;
        private System.Windows.Forms.Button buImageSave;
        private System.Windows.Forms.Button buImageClear;
        private System.Windows.Forms.Button buColorDialog;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnEraser;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}

